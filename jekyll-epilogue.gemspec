# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-epilogue"
  spec.version       = "0.1.2"
  spec.authors       = ["Chaitali Patil"]
  spec.email         = ["cpatil@saiashirwad.com"]

  spec.summary       = "A slick landing page with clean lines and a minimalistic look and feel."
  spec.homepage      = "https://cpatil.gitlab.io/Jekyll-Epilogue/"
  spec.license       = "MIT"

  spec.files = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^(_(includes|layouts|sass)|(contact|elements|LICENSE|README)((\.(txt|md|markdown|html)|$)))}i)
  end
#  spec.files += Dir.glob('bower_components/**/*')

  spec.add_development_dependency "jekyll", "~> 3.2"
  spec.add_development_dependency "jekyll-bower", "~> 1.1.0"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.post_install_message = "Thanks for Installing jekyll-epilogue by Sai Ashirwad Informatia & Design by Templated.co!!"
end